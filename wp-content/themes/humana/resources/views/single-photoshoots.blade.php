{{--
  Page for single job
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')

<div class="container">
	<div class="entry-content animate animate__fade-up">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>
</div>

@endsection