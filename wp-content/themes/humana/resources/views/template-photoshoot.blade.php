{{--
  Template Name: Photoshoot
--}}

@extends('layouts.app')
@section('content')
@include('layouts.page-header')

<div class="container">
	<div class="ps">
		<?php
		global $post;
		$args = array( 
			'posts_per_page' => -1,
			'post_type' => 'photoshoots'
		);
		$posts = get_posts( $args );

		foreach ( $posts as $post ) : setup_postdata( $post ); ?>
				<?php 
					$images = rwmb_meta( 'ps_image', array( 'size' => 'large' ) );
					$count = count($images);
					$doubleClass = "";
					if ($count > 1) :
						$doubleClass = " ps-item__image__double";
					endif;
				?>
				<div class="ps-item">
					<div class="row">

						<div class="col-sm-8">
							<?php foreach ( $images as $image ) : ?>
								<div class="ps-item__image animate animate__fade-up<?php echo $doubleClass; ?>" style="background-image: url('<?php echo $image['url'] ?>');"></div>
							<?php endforeach; ?>
						</div>

						<div class="col-sm-4">
							<a href="<?php echo the_permalink(); ?>">
								<h2 class="ps-item__title animate animate__fade-up"><?php the_title(); ?></h2>
							</a>
							<a href="<?php echo the_permalink(); ?>" class="button--read-more animate animate__fade-up"><?php echo pll__('Look more', 'Buttons'); ?></a>
						</div>

					</div>
				</div>
			
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
	</div>
</div>

@endsection