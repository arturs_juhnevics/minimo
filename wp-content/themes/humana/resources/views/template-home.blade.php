{{--
  Template Name: Home
--}}
@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.home.index')
    
    {{-- @include('partials.home.category')
    @include('partials.home.solutions')
    @include('partials.home.facts')
    @include('partials.home.partners') --}}
  @endwhile
@endsection