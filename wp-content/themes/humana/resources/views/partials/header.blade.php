@php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
$lang_args = array(
	'show_names' => 1,
	'show_flags' => 0, 
);
@endphp
<header class="">
<nav class="navigation">
	<div class="main-nav-container">
		<div class="container">
			<div class="menu-overlay"></div>
			<a class="navbar-brand" href="#"><img src="{{ $header_image }}" alt="Humana"></a> 
			<div class="menu">
				<div class="side-menu">
					<ul class="side-menu__list">
						<li class="side-menu__list__item active" data-page="1">intro.</li>
						<li class="side-menu__list__item" data-page="2">work.</li>
						<li class="side-menu__list__item" data-page="3">contact.</li>
					</ul>
				</div>
			</div>
		  	<div class="lang_menu">
		  		<?php pll_the_languages($lang_args); ?>
		  	</div>
		  	<button class="hamburger hamburger--squeeze" type="button">
			  	<span class="hamburger-box">
			    	<span class="hamburger-inner"></span>
			  	</span>
			</button>
		</div>

	</div>
</nav>
</header>