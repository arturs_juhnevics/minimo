<div @php post_class() @endphp>
  <?php 
$url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
  ?>
<div class="container">

  <div class="row">

    <div class="col-sm-2 d-none d-lg-block">
       <div class="share">
        <p><?php echo pll__('Share this article', 'General'); ?></p>
        <a class="share__icon" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&src=sdkpreparse" <?php echo file_get_contents(get_template_directory_uri()."/assets/images/faceboook.svg"); ?></a>
      </div>
    </div>
   
  <div class="col-sm-10 ">
    <div class="entry-content">
      <div class="post-featured-image">
        <img alt='<?php echo get_the_title(); ?>' src='<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large') ?>'>
      </div>
      @php the_content() @endphp
    </div>
  </div> 

  </div>
    <div class="related-posts">
      <div class="related-posts__nav">
        <h2><?php echo pll__('You might be also interested in', 'Post') ?></h2>
        <div class="related-posts__nav__controls slick-controls">
          <span class="arrow-left"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-left.svg"); ?></span>
          <span class="arrow-right"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-right.svg"); ?></span>
        </div>
      </div>
      <div class="post-slider">
        <?php 
          $query = new WP_Query( 
            array( 
              'post_type' => 'post',
              'posts_per_page'=> 6, 
              'post__not_in' => array(get_the_ID()),
            ) 
          );
          ?>
          <?php while ($query->have_posts()) : $query->the_post(); ?> 
            @include('partials.related-posts')
          <?php endwhile; ?>
      </div>
    </div>

  </div>

</div>
