<head>
  <?php 
    $before_head = rwmb_meta( 'before_head', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings'); 
    echo $before_head;
  ?>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="minimo.lv">
  <meta property="og:url"           content="<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="<?php echo get_the_title(); ?>" />
  <meta property="og:description"   content="" />
  <meta property="og:image"         content="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>" />
  @php wp_head() @endphp

  <?php
  $favis = rwmb_meta( 'favicon', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $favi = reset( $favis );
  ?>
  <?php if ( $favi ) : ?>
      <link rel="shortcut icon" href="<?php echo $favi['url']; ?>">
      <link rel="icon" href="<?php echo $favi['url']; ?>">
  <?php endif; ?>
</head>
