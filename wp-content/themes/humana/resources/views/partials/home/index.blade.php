
	<div class="section" id="intro">
		<div class="container">
			<div class="hero">
				<h1>WE DEVELOP WEBSITES TAILORED FOR YOUR BUSINESS</h1>
				<p>We are not an digital agency, We are group of web experts, helping our clients to deliver the best design for their company needs. We develop our websites on Wordpress CMS making it easy and intuitive to use. </p>
			</div>
		</div>
	</div>

	<div class="section portfolio" id="work">
		<div class="container">
			<div class="portfolio__item">
				<div class="row">
					
					<div class="col-sm-6 push-sm-6">
						<div class="portfolio__item__info">
							<div class="portfolio__title">
								<h1>HUMANA</h1>
								<a class="button--read-more" href="http://www.humana.lt" target="_blank">Visit website</a>
							</div>
							<p>
							Website redesign for the popular second hand clothing store - Humana. They work around the world helping people by delivering them fashionable and sustainable clothing at best price. 
							<p>
								<p>
									We developed for them a custom designed website built on Wordpress CMS. Website is fully mobile device friendly. It includes functionality such as "Store locator" to simply locate nearest store unsing interactive map. Other pages are neat and simple for customer and client to use.
								</p>

								<p>Visit the website to view the work.</p>
						</div>
					</div>
					<div class="col-sm-6 pull-sm-6">
						<div class="portfolio__item__image">
							<img src="<?php echo get_template_directory_uri() . "/assets/images/humana.png"; ?>">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section" id="contact">
		<div class="container">
			<div class="contact">
				<h1>GET IN TOUCH</h1>
				<p>If you are looking for custom designed webiste with custom funcionality write us for more information and price</p>
				@include('partials.home.getintouch')
			</div>
		</div>
	</div>

