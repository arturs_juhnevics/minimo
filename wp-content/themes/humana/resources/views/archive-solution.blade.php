@extends('layouts.app')

@section('content')
 @include('layouts.page-header')

  <div class="container solution-list row">
   @while(have_posts()) @php the_post() @endphp
  		@include('partials.solutions.solution-list')
   @endwhile
 </div>
@endsection