  <!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
      <div class="wrapper">

        <div class="main">
          @yield('content')
        </main>
      </div>
    @php do_action('get_footer') @endphp
     <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.0.3"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    @php wp_footer() @endphp
</html>
