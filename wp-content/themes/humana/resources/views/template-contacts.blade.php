{{--
  Template Name: Contacts
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')
  @while(have_posts()) @php the_post() @endphp
<div class="container contacts">
  <div class="contacts__offices">
    <div class="row">
          <?php $offices = rwmb_meta('locations_contacts'); ?>
          <?php foreach ($offices as $item) { ?>
            <?php 
             $image_ids = $item['offices_icon'];
             $image = RWMB_Image_Field::file_info( $image_ids[0], array( 'size' => 'medium' ));
            ?>

            <div class="col-sm-4">
              <div class="contacts__offices__item">
                <div class="contacts__offices__title">
                    <span class="animate animate__fade-up"><?php echo file_get_contents($image['url']); ?></span>
                   <h2 class="animate animate__fade-up"><?php echo $item['offices_title']; ?></h2>
                </div>
               
                <div class="contacts__offices__info animate animate__fade-up"><?php echo $item['offices_info']; ?></div>
                </div>
            </div>
          <?php } ?>
        </div>
  </div>
</div>

<div class="contact-section2">
  <div class="contact-section2-content">
    <div class="contact-section2-content__text"></div>
    <div class="map animate animate__fade-up">
        <?php
        $map = rwmb_get_value('contact_map');
        $pins = rwmb_meta('contact_map_pin', array( 'limit' => 1 ));
        $pin = reset( $pins );

        ?>
        <?php if (!empty($map)) : ?>
            <div class="contacts__map">
              <div id="map-wrap"
                     class="contacts__map__wrap"
                       data-lng="<?php echo $map['longitude'] ?>"
                     data-lat="<?php echo $map['latitude'] ?>"
                     data-zoom="<?php echo $map['zoom']; ?>"
                     data-pin="<?php echo $pin['url']; ?>">
                       
              </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <!-- contact form -->
             <div class="contacts__form">
                  <form id="contact-form" method="POST">
                      <h2 class="contacts__title"><?php echo pll__('Contact form', 'Contact-form'); ?></h2>
                      <div class="row">
                          <div class="col-sm-6 animate animate__fade-up">
                              <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Your name', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="name" id="name" class="required"/>
                              </label>
                              <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Your email address', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="phone" id="phone" class="required"/>
                              </label>
                          </div>
                          <div class="col-sm-6 animate animate__fade-up">
                              <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Name of the company', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="company" id="company" class="required"/>
                              </label>
                              <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Telephone', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="email" id="email" class="required"/>
                              </label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12 animate animate__fade-up">
                             <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Subject of text', 'Contact-form'); ?><span class="req">*</span></span>
                                  <textarea name="message" id="message" > </textarea>
                              </label>
                          </div>
                      </div>
                          <div id="recaptcha_element"></div>
                          <div class="col-sm-12 button-container" style="text-align:center;">
                              <div class="form__status hidden"></div>
                              <button class="button" class="contacts__form__submit"><?php echo pll__('Send', 'Contact-form'); ?></button>
                          </div>
                          <div class="col-sm-12">
                              <?php wp_nonce_field('contact-nonce', 'contact-nonce'); ?>
                              <?php $from = '';
                              if (isset($_GET['from'])) {
                                  $from = strip_tags($_GET['from']);
                              } ?>
                              <input type="hidden" name="page_from" id="page_from" value="<?php echo $from; ?>" class="leave"/>
                              <input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID; ?>" class="leave"/>
                              <input type="hidden" name="action" id="action" value="contact_submit" class="leave"/>
                              <input type="text" name="contact-honey" id="contact-honey" class="hidden"/>
                              
                          </div>
                  </form>
              </div>

          <!-- contact form -->
        </div>
        <div class="col-sm-6">
        </div>
    </div>
  </div>
</div>



  @endwhile
  
@endsection


   			