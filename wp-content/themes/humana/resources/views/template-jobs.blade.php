{{--
  Template Name: Job offers
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')

<div class="container">
<div class="job_qoute animate animate__fade-up">
	
	<p><?php echo pll__('Are you intrigued and want to become part of our team? Check out vacancies below or send us a message!', 'Job-page'); ?></p>
</div>

<div class="job-listing">
	<p class="job-listing__title animate animate__fade-up"><?php echo pll__('We are currently looking for the following people:', 'Job-page'); ?></p>

	<?php
	global $post;
	$args = array( 
		'posts_per_page' => -1,
		'post_type' => 'jobs'
	);
	$posts = get_posts( $args );

	foreach ( $posts as $post ) : setup_postdata( $post ); ?>
		<div class="job-listing__item animate animate__fade-up">
			<a href="<?php echo the_permalink(); ?>" class="job-listing__item__title" > <?php the_title(); ?></a>
			<a href="<?php echo the_permalink(); ?>" class="button--read-more"><?php echo pll__('Read more', 'Buttons'); ?></a>
		</div>
	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
</div>
</div>
@endsection