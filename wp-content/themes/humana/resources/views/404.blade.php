@extends('layouts.app')

@section('content')
    @include('layouts.page-header')

  @if (!have_posts())
    <div class="container">
        <h2 style="text-align: center;">
      {{ __('Sorry, but the page you were trying to view does not exist.', '404') }}
        </h2>
    </div>
  @endif
@endsection
