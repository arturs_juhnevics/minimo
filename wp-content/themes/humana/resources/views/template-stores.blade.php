{{--
  Template Name: Store Finder
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')
<?php while (have_posts()) : the_post(); ?>
    <div class="smartmap">
        <?php get_template_part('templates/page', 'header'); ?>
        <div class="container">
            <div id="map-wrap" class="smartmap__map" data-pin="<?php echo get_template_directory_uri(); ?>/assets/img/icon-pin.png"></div>
            <div class="smartmap__filters clearfix animate animate__fade-up">
                <div class="smartmap__filter smartmap__filter--city">
                    <span class="smartmap__filter__prefix"><?php  echo pll__('City:', 'Stores'); ?></span>
                    <select name="city">
                        <option value="0all"><?php  echo pll__('All cities', 'Stores'); ?></option>
                    </select>
                </div>
            </div>
            <div class="clearfix">
                <div class="smartmap__seller">
                    <div class="smartmap__seller__inner">
                        <div class="smartmap__seller__head">
                            <div class="smartmap__seller__distance">Attālums: <span class="number loading"></span></div>
                            <div class="smartmap__seller__directions"><a href="#" title="" target="_blank"><?php  echo pll__('Plan route', 'Stores'); ?></a></div>
                        </div>
                        <h3 class="smartmap__seller__title">Humana Pylmo STR</h3>
                        <div class="smartmap__seller__address">Pylimo str. 45 / Gėlių str. 1, Vilnius</div>
                         <div class="smartmap__seller__hours">Hours</div>
                        <div class="smartmap__seller__close"><?php echo pll__('Close', 'Stores'); ?><span></span></div>
                        <div class="smartmap__seller__name ">Violeta Likauskienė</div>
                        <div class="smartmap__seller__info smartmap__seller__info--phone">+370 (5) 212 0994</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php 
$args = array('post_type' => 'store', 'posts_per_page' => -1);
$posts = new WP_Query( $args ); 
$stores = array();
$count = 1;
while($posts->have_posts()) : $posts->the_post();
    $city = rwmb_meta( 'city', $post->ID ); 
    $seller = rwmb_meta( 'contact_person', $post->ID ); 
    $address = rwmb_meta( 'address', $post->ID ); 
    $phone = rwmb_meta( 'phone', $post->ID ); 
    $hours = rwmb_meta( 'hours', $post->ID ); 
    array_push(
        $stores, 
        array(
            'id' => $count,
            'city' => $city,
            'address' => $address,
            'seller' => $seller,
            'phone' => $phone,
            'hours' => $hours,
           
        )
    );
$count++;
endwhile;

$groupedStores = array();

foreach($stores as $val) {
    if(array_key_exists('city', $val)){
        $groupedStores [$val['city']][] = $val;
    }else{
        $groupedStores [""][] = $val;
    }
}

$count = 1;
$acccount = 0;
?>
<div class="store-list">
    <div class="container">
        <div class="store-list__container accordion" id="accordionExample">
        <?php foreach ($groupedStores as $key => $value) : ?>
            <div id="city<?php echo $acccount; ?>" class="store-list__heading-wrapper">
              <h2 class="store-list__heading">
                <button type="button" data-toggle="collapse" data-target="#collapse<?php echo $acccount; ?>" aria-expanded="false" aria-controls="collapse<?php echo $acccount; ?>">
                 <?php echo $key; ?>
                 <span class="plus-icon"></span>
                </button>
              </h2>
            </div>

            <div id="collapse<?php echo $acccount; ?>" class="collapse store-list__items" aria-labelledby="city<?php echo $acccount; ?>" data-parent="#accordionExample">
                <div class="row">
                    <?php foreach ($value as $item) : ?>
                        <div class="col-sm-4 store-list__item-wrapper">
                            <div class="store-list__item">
                               
                                <p class="store-list__address"><?php echo $item['address']; ?></p>
                                <p class="store-list__hours"><?php echo $item['hours']; ?></p>
                                <p class="store-list__seller"><?php echo $item['seller']; ?></p>
                                <a href="tel:<?php echo $item['phone']; ?>" class="store-list__phone"><?php echo $item['phone']; ?></a>
                                <a class="button--read-more view-on-map" data-id="<?php echo $item['id']; ?>"><?php echo pll__('View on map', 'Buttons'); ?></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php $acccount++; ?>
        <?php endforeach; ?>
    </div>
</div>
    @endsection
<?php endwhile; ?>
<script type="text/javascript">
    var locations = [
<?php while($posts->have_posts()) : $posts->the_post(); ?>

<?php 
$title = rwmb_meta( 'store_title', $post->ID );
$city = rwmb_meta( 'city', $post->ID ); 
$seller = rwmb_meta( 'contact_person', $post->ID ); 
$address = rwmb_meta( 'address', $post->ID ); 
$phone = rwmb_meta( 'phone', $post->ID ); 
$hours = rwmb_meta( 'hours', $post->ID ); 
$map = rwmb_get_value('map', $post->ID);
?>
            {
                id: '<?php echo $count; ?>',
                name: '<?php echo $title; ?>',
                lat: '<?php echo $map["latitude"]; ?>',
                lng: '<?php echo $map["longitude"]; ?>',
                city: '<?php echo $city; ?>',
                seller: '<?php echo $seller; ?>',
                address: '<?php echo $address; ?>',
                phone: '<?php echo $phone; ?>',
                hours: '<?php echo $hours; ?>',

            },

<?php $count++; ?>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
    ];
</script>