<?php
add_filter( 'rwmb_meta_boxes', 'contacts_meta_boxes' );
function contacts_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'General',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
             array(
                'id' => 'locations_contacts',
                'clone' => true,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => "Offices",
                'fields' => array(
                      array(
                        'id'               => 'offices_icon',
                        'name'             => 'Icon',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
                    array(
                        'id'   => 'offices_title',
                        'name' => 'Title',
                        'type' => 'text',
                        'size' => '60',
                    ),            
                    array(
                        'id'   => 'offices_info',
                        'name' => 'Info',
                        'type' => 'WYSIWYG',
                        'size' => '60',
                    ),
                )
            ),
           
        ),
        $meta_boxes[] = array(
            'title'      => 'Map',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-contacts.blade.php')
            ),
            'name' =>'map',
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(

                array(
                    'id'   => 'address',
                    'name' => 'Address',
                    'type' => 'text',
                    'placeholder' => 'Address',
                    'size' => '80',
                ),
                array(
                    'id'            => 'contact_map',
                    'name'          => 'Location',
                    'type'          => 'map',
                    'address_field' => 'address',
                    'api_key'       => 'AIzaSyBaob1XUk6NQ7RmmsB4QuhqjGh3foYE3EE',
                ),
                array(
                    'name' => 'Pin',
                    'id'   => 'contact_map_pin',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                ),
                array(
                    'name' => 'Contact form active',
                    'id'   => 'cform_active',
                    'type' => 'checkbox',
                    'std'  => 1,
                ),
                array(
                    'id'   => 'contact_email',
                    'name' => 'Contact Email',
                    'type' => 'text',
                    'placeholder' => 'Contact email',
                    'size' => '50',
                    'visible' => array( 'cform_active', true )
                ),

        
            )
        )
    );
    return $meta_boxes;
}