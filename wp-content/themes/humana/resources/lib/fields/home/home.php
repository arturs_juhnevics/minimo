<?php
add_filter( 'rwmb_meta_boxes', 'home_meta_boxes' );
function home_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
            'title'      => 'Special offers (Left side)',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-home.blade.php')
            ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                 array(
                        'id'               => 'offer_bg_left',
                        'name'             => 'Background image',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
                array(
                    'id'   => 'offer_left_text1',
                    'name' => 'Text 1',
                    'type' => 'text',
                    'size' => '60',
                ),
                 array(
                    'id'   => 'offer_left_text2',
                    'name' => 'Text 2',
                    'type' => 'text',
                    'size' => '60',
                ),
                array(
                    'id'   => 'sp_offers_table_left',
                    'name' => 'Offer table',
                    'type' => 'WYSIWYG',
                ),
            ),
        );
     $meta_boxes[] = array(
            'title'      => 'Special offers (Right side)',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-home.blade.php')
            ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                 array(
                        'id'               => 'offer_bg_right',
                        'name'             => 'Background image',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
                 array(
                    'id'   => 'offer_right_text1',
                    'name' => 'Text 1',
                    'type' => 'text',
                    'size' => '60',
                ),
                 array(
                    'id'   => 'offer_right_text2',
                    'name' => 'Text 2',
                    'type' => 'text',
                    'size' => '60',
                ),
                array(
                    'id'   => 'sp_offers_table_right',
                    'name' => 'Offer table',
                    'type' => 'WYSIWYG',
                ),
            ),
        );
    $meta_boxes[] = array(
            'title'      => 'About Us',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-home.blade.php')
            ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                array(
                    'id'   => 'about_text',
                    'name' => 'Title',
                    'type' => 'WYSIWYG',
                    'size' => '60',
                ),
            ),
        );
     $meta_boxes[] = array(
            'title'      => 'Call to action',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-home.blade.php')
            ),
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(
                array(
                    'id'   => 'cta-text',
                    'name' => 'Title',
                    'type' => 'text',
                    'size' => '60',
                ),
                array(
                    'id'   => 'cta_button',
                    'name' => 'Button',
                    'type' => 'text',
                    'size' => '60',
                ),
                array(
                    'id'   => 'cta_button_url',
                    'name' => 'Button URL',
                    'type' => 'text',
                    'size' => '60',
                ),
                 array(
                        'id'               => 'cta_bg',
                        'name'             => 'Background image',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
            ),
        );
    return $meta_boxes;
}