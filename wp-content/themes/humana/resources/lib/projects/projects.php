<?php

class Projects{

    public function __construct() {

        $labels = array(
            'name'               => "Project",
            'singular_name'      => "Project",
            'menu_name'          => "Project",
            'add_new_item'       => "Add Project",
            'new_item'           => "New Project",
            'edit_item'          => "Edit Project",
            'view_item'          => "View Project",
            'all_items'          => "All Projects",
            'search_items'       => "Search Projects",
            'not_found'          => "No Project found",
            'not_found_in_trash' => "No Project found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => false,
            'supports' => array('thumbnail', 'title'),   

            'show_in_graphql' => true,
            'graphql_single_name' => 'Project',
            'graphql_plural_name' => 'Projects', 
           
        );

        register_post_type('project', $args);

       
    }

}
new Projects();
