<?php
ob_start();
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

use Roots\Sage\Config;
use Roots\Sage\Container;

$roots_includes = array(
    'lib/projects/projects.php', // Projects
    'lib/breadcrumbs.php', // Products
    'lib/extras.php', // Extras, Widget area's,
    'lib/disable-gb.php',     // disable Gutenberg on certain tempaltes
    'lib/admin.php',        // Rearrange admin fields and menus
    'lib/translations.php', // Generated Polylang string translations
    'lib/options.php', // ACF options page
    //'lib/graphql/resolvers.php', // Graphql menu endpoints
);

foreach ($roots_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'roots'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

add_action('wp_AJAX_svg_get_attachment_url', 'get_attachment_url_media_library');

function get_attachment_url_media_library(){

    $url = '';
    $attachmentID = isset($_REQUEST['attachmentID']) ? $_REQUEST['attachmentID'] : '';
    if($attachmentID){
        $url = wp_get_attachment_url($attachmentID);
    }

    echo $url;

    die();
}

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);

add_action('wp_ajax_submit_form', 'submit_form');
add_action('wp_ajax_nopriv_submit_form', 'submit_form');

 function submit_form()
    {
        if (isset($_POST['contact-nonce']) && wp_verify_nonce($_POST['contact-nonce'],
                'contact-nonce') && $_POST['contact-honey'] === ''
        ) {

            add_filter('wp_mail_content_type', function () {
                return 'text/html';
            });

            $data = array(
                'name'    => strip_tags($_POST['name']),
                'email'   => strip_tags($_POST['email']),
                'message' => strip_tags($_POST['message']),
            );

            $output = '<h3>Jauna ziņa no '.get_bloginfo("name").' kontaktformas</h3>';
            $output .= 'Vārds: '    . $data['name']    . '<br />';
            $output .= 'Epasts: '   . $data['email']   . '<br />';
            $output .= 'Ziņa: '     . $data['message'];

           

         
                foreach ($data as $key => $value) {
                    $data[$key] = json_encode($value);
                }
                $df_form = new Df_Forms();
                $df_form->insert($data, 'Contact form');

            $headers = 'From: Contact Form Minimo' . "\r\n";
            $email = 'arturs@minimo.lv';
            if ($email) {
                $sent = wp_mail($email, __('Message from contact form'), $output, $headers);
            } else {
                $sent = false;
            }

                $response = array(
                    'message' => pll__('We have recieved your message!')
                );
                wp_send_json_success($response);
           
        }

        die;
    }

add_action('wp_ajax_submit_newsletter', 'submit_newsletter');
add_action('wp_ajax_nopriv_submit_newsletter', 'submit_newsletter');

function submit_newsletter(){
    $url = "https://app.sender.net/api/";

    $email = $_POST['email'];
    $listid = $_POST['listid'];
    $data = array(
        "method" => "listSubscribe", 
        "params" => [
            "api_key" => "b8b21c38a6882003134c44ec83de5366",
            "list_id" => $listid,
            "emails" => [
                $email
            ]
        ]
    );

    $options = [
        'http' => [
            'method'  => 'POST',
            'content' => http_build_query(array('data' => json_encode($data)))
        ]
    ];

    $context = stream_context_create($options);
$result = file_get_contents($url, false, $context);

wp_send_json_success($result);
}
    
function my_js_variables()
{
    ?>
    <script>
        var ajaxurl = <?php echo json_encode( admin_url( "admin-ajax.php" ) ); ?>,
            base_url = <?php echo json_encode( get_template_directory_uri() ); ?>,
            template_url = <?php echo json_encode( get_stylesheet_directory_uri() ); ?>,
            translations = {
                required    : '<?php echo pll__("Please fill all required fields", "Error Messages") ?>',
                valid_email : '<?php echo pll__("Please provide a valid email address", "Error Messages") ?>',
                valid_captcha : '<?php echo pll__("Please check the reCAPTCHA field", "Error Messages") ?>',
                newsletter_added : '<?php echo pll__("You have been added to subscriber list", "Newsletter") ?>',
                newsletter_error : '<?php echo pll__("You have already subscribed", "Newsletter") ?>',
                select : '<?php echo pll__("Choose") ?>',
                prev: '<?php echo pll__("Previous") ?>',
                next : '<?php echo pll__("Next") ?>',
                readmore : '<?php echo pll__("Lasīt vairāk") ?>',
                terms: '<?php echo pll__("You need to agree to Terms & conditions", "Error Messages") ?>'
            },
            gmap_api_key = "AIzaSyCXCYNdBKHz7wJEJO0KHVkqr-MsSDBezk8";
    </script><?php
}
if(function_exists('pll__')) add_action('wp_print_scripts', 'my_js_variables');

add_filter('login_headertitle', 'my_login_logo_url_title');

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display()
{
    echo '<style>
    td.media-icon img[src$=".svg"], .image img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      max-width: 100px !important; 
      height: auto !important; 
    }
  </style>';
}

add_action('admin_head', 'fix_svg_thumb_display');

/**
 * Hide all notices
 */
function hide_wp_notice() {
    remove_all_actions( 'admin_notices' );
}
add_action( 'admin_head', 'hide_wp_notice', 1 );    

ob_end_clean();