/** @license
 * RequireJS plugin for async dependency load like JSONP and Google Maps
 * Author: Miller Medeiros
 * Version: 0.1.2 (2014/02/24)
 * Released under the MIT license
 */
define(function(){var f=0;return{load:function(e,t,n,r){if(r.isBuild)n(null);else{var a="__async_req_"+(f+=1)+"__";window[a]=n,s=t.toUrl(e),o=a,d=/!(.+)/,p=s.replace(d,""),u=d.test(s)?s.replace(/.+!/,""):"callback",c=(p+=p.indexOf("?")<0?"?":"&")+u+"="+o,(i=document.createElement("script")).type="text/javascript",i.async=!0,i.src=c,(l=document.getElementsByTagName("script")[0]).parentNode.insertBefore(i,l)}var c,i,l,s,o,d,p,u}}});