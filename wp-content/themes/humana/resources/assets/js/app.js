requirejs.config({
    baseUrl: base_url + '/assets/js/modules',
    urlArgs: "v=1.0.6",
    waitSeconds : 30,
    paths: {

        // plugins
        'async'             : '../plugins/async',
        'slick'             : '../plugins/slick.min',
        'jquery'            : '../plugins/jquery3.3.1.min',
        'selectize'         : '../plugins/selectize.min',
        'bootstrap'         : '../plugins/bootstrap.bundle.min',
        'cooqui'         : '../plugins/jquery-cabg-cooqui',


        // modules
        'common'                            : 'min/common',
        'page_template_template_contacts'   : 'min/contacts',
        'page_template_template_stores'     : 'min/stores',
        'single_product'                    : 'min/product',


    },

    shim: {
        'cooqui': {
            deps: ['jquery']
        },
        'pagescroll': {
            deps: ['jquery']
        },
        'common': {
            deps: ['async', 'jquery', 'slick', 'bootstrap', 'cooqui']
        },
        'page_template_template_contacts' : {
            deps : ['async']
        },
        'page_template_template_stores': {
            deps: ['selectize']
        },

    }
});

requirejs(['common'], function() {
    (function($) {
        var UTIL = {
            loadEvents: function() {
                common.init();
                $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                    if (typeof(requirejs.s.contexts._.config.paths[classnm]) != 'undefined') {
                        requirejs([classnm], function() {

                            if (typeof(this[classnm]) !== 'undefined') {
                                if (typeof(this[classnm].finalize) !== 'undefined') {
                                    this[classnm].finalize();
                                }
                            }
                        });
                    }
                });
            }
        };
        // Load Events
        $(document).ready(UTIL.loadEvents);
    })(jQuery); // Fully reference jQuery after this point.
});