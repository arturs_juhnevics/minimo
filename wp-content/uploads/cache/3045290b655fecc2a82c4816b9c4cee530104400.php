<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php if(!have_posts()): ?>
    <div class="alert alert-warning">
      <?php echo e(__('Sorry, no results were found.', 'sage')); ?>

    </div>
    <?php echo get_search_form(false); ?>

  <?php endif; ?>
<?php 
  $terms = get_categories( 
    array(
      'hide_empty' => true,
    ) 
  ); 
  $blog_page = get_option( 'page_for_posts' );
  $object = get_queried_object();
?>
<div class="container">
  <div class="post_terms">
    <ul class="post_terms__list">
      <?php 
      $active = '';
      if( $blog_page == $object->ID ) {
        $active = ' active';
      }
      ?>
      <li class="post_terms__list__item animate animate__fade-up<?php echo $active; ?>"><a href='<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>'><?php echo pll__('All', 'General'); ?></a></li>
      <?php foreach ($terms as $term) : ?>
        <?php 
        $active = '';
        if( $term->term_id == $object->term_id ) {
          $active = ' active';
        }
        ?>
        <li class="post_terms__list__item animate animate__fade-up<?php echo $active; ?>"><a href='<?php echo get_term_link( $term->term_id ); ?>'><?php echo $term->name; ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
  <div class="row">
    <?php while(have_posts()): ?> <?php the_post() ?>
      <?php echo $__env->make('partials.content-'.get_post_type(), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endwhile; ?>
  </div>
</div>
  

  <?php echo get_the_posts_navigation(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>