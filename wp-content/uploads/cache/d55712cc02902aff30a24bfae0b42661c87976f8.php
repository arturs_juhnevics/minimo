  <!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body <?php body_class() ?>>
    <?php do_action('get_header') ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="wrapper">

        <div class="main">
          <?php echo $__env->yieldContent('content'); ?>
        </main>
      </div>
    <?php do_action('get_footer') ?>
     <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.0.3"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    <?php wp_footer() ?>
</html>
