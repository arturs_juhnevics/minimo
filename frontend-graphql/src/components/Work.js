import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import ProjectItems from './Project-items';


class Work extends Component {

  render() {
    return (
      <div className="work-page">
        <div className="page-content">
           <h2 className="page-title">Work</h2>
           <p>Hey, these are our projects that we have worked on, feel free to browse them and if you feel like you have found the right agency for you, then dont hesitate and contact us at <a className="email" href="mailto:arturs@minimo.lv">arturs@minimo.lv</a></p>
        </div>
       <div className="projects">
          <ProjectItems />
        </div>
      </div>
    );
  }
}

export default withApollo(Work);
