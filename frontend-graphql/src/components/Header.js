import React, { Component } from 'react';
import DelayLink from './elements/DelayLink';
import { withRouter } from 'react-router';
import { withApollo } from 'react-apollo';
import { compose } from 'recompose';
import ReactDOM from 'react-dom';
import { ReactComponent as Logo } from '../static/images/minimo.svg';
import BurgerButton from './elements/BurgerButton';
import Navigation from './Navigation';
import store from '../store';
import { navState } from '../actions/navigation';
import { connect } from 'react-redux';


class Header extends Component {

  constructor(props, context) {
    super(props, context);
     
    this.state = {
      visible: false,
      scrolled: false,
    };
     
    this.toggleMenu = this.toggleMenu.bind(this);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  toggleMenu() {
    this.props.navState(true); 
  }

  handleMouseDown(e) {
    this.toggleMenu();
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside, true);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
      document.removeEventListener('click', this.handleClickOutside, true);
      window.removeEventListener('scroll', this.handleScroll);
  }

  handleClickOutside = event => {
      const domNode = ReactDOM.findDOMNode(this);

      if (!domNode || !domNode.contains(event.target)) {
        this.props.navState(false); 
      }
  }
  handleClose = event => {
    this.props.navState(false); 
}

  handleScroll = event => {
    var top = (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0);
    if (top > 100) {
      this.setState({ 
        scrolled: true
      });
    }else{
      this.setState({ 
        scrolled: false
      });
    }
  }

  render() {
    const storeState = store.getState();
    console.log(storeState);
    const { scrolled, visible } = this.state;
    return (
      <div className={[scrolled ? "scrolled" :  "", this.props.loading ? "loading" : "", "header"].join(' ')}>
        <div className="container">
          <div className="burger">
            <BurgerButton handleMouseDown={this.handleMouseDown} />
            <Navigation menuVisibility={storeState.page.navClosed} handleClose={this.handleClose}/>
          </div>
          <div className="logo">
            <DelayLink className="link" delay={900} to="/">
              <h1 className="logo-font">minimo<span className="dot">.</span></h1>
            </DelayLink>
          </div>
          <div className="langMenu"> 
          </div>
        </div>
      </div>
    );
    
  }

}
const mapStateToProps = state => ({
  page: state.page
})

export default withRouter( connect(mapStateToProps, { navState })(Header))
