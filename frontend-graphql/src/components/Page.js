import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';


import { connect } from 'react-redux';
import { pageLoading } from '../actions/pageActions';

/**
 * GraphQL page query that takes a page slug as a uri
 * Returns the title and content of the page
 */
const PAGE_QUERY = gql`
  query PageQuery($uri: String!) {
    pageBy(uri: $uri) {
      pageId
      title
      content
    }
  }
`;

/**
 * Fetch and display a Page
 */
class Page extends Component {
  state = {
    page: {
      title: '',
      content: '',
    },
  };

  componentDidMount() {
    this.executePageQuery(); 
  }

  componentDidUpdate(nextProps) {
    if(this.props.match.url !== nextProps.match.url) {
      this.executePageQuery(); 
    }
  }
  /**
   * Execute page query, process the response and set the state
   */
  executePageQuery = async () => {
    const { match, client } = this.props;
    let uri = match.params.slug;
    if (!uri) {
      uri = 'welcome';
    }
    const result = await client.query({
      query: PAGE_QUERY,
      variables: { uri },
    });
    const page = result.data.pageBy;
    this.setState({ page });
    this.props.pageLoading(false); 
   
  };

  render() {
    const { page } = this.state;

    return (
      <div className={`page-content post-${page.pageId}`}>
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: page.content,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  page: state.page
})

export default withApollo( connect(mapStateToProps, { pageLoading })(Page));
