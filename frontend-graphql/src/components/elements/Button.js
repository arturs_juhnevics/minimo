import React, { Component } from 'react';

class Button extends Component {

	
	render() {
		var url = this.props.href;
		console.log(url);

		if( url === null ){
			return("")
		}else{
			return (
				<a target={this.props.target} className="button" href={this.props.href}>{this.props.name}</a>
			)
		}
		
	}
}

export default Button;