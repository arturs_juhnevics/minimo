import React, { Component } from 'react';
import { ReactComponent as Burger } from '../../static/images/burger.svg';

class BurgerButton extends Component {

	
	render() {
		return (
			<button onMouseDown={this.props.handleMouseDown}><Burger/></button>
		)
	}
}

export default BurgerButton;