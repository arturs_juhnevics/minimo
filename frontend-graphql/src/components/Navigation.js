import React, { Component } from 'react';
import DelayLink from './elements/DelayLink';
import { withRouter } from 'react-router';
import { withApollo } from 'react-apollo';
import { compose } from 'recompose';
import gql from 'graphql-tag';
import { ReactComponent as Close } from '../static/images/cancel.svg';

/**
 * GraphQL menu query
 * Gets the labels, types (internal or external) and URLs
 */
const MENU_QUERY = gql`
  query {menuItems(where: {location: PRIMARY_NAVIGATION}) {
      edges {
        node {
          url
          label
        }
      }
    }
  }
`;


class Navigation extends Component {
  state = {
    menus: [],
  };

  componentDidMount() {
    this.executeMenu();
  }

  /**
   * Execute the menu query, parse the result and set the state
   */
  executeMenu = async () => {
    const { client } = this.props;
    const result = await client.query({
      query: MENU_QUERY,
    });

    const menus = result.data.menuItems.edges;
    this.setState({ menus });
  };


  render() {

    const { menus } = this.state;

    var visibility = "navigation__hide";
 
    if (this.props.menuVisibility) {
      visibility = "navigation__show";
    }

    return (

        <div className={ visibility + " navigation"} 
            onMouseDown={this.props.handleMouseDown} >
            <div onClick={this.props.handleClose} className="close-button"><span>{<Close />}</span></div>
            <div className="menu">
              {menus.map(menu => (
                      
                  <DelayLink
                    key={menu.node.label}
                    to={"/page/"+menu.node.label}
                    className="navigation__item link"
                    delay={900}
                  >
                    {menu.node.label}
                  </DelayLink>

              ))}
            </div>
        </div>

    );
  }
}

export default compose(
  withRouter,
  withApollo,
)(Navigation);
