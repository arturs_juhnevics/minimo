import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import Button from './elements/Button';
import ProjectItems from './Project-items';
import DelayLink from './elements/DelayLink';
import { connect } from 'react-redux';
import { pageLoading } from '../actions/pageActions';

class Home extends Component {
  
  componentDidMount() {

  }

  render() {
    return (
      <div className="container">
        <div className="hero">
          <h1>Hi, we are a web design studio,
          we are a group of freelancers 
          that will always give their best 
          to deliver you and your company
          the website you have been waiting
          for</h1>
          <div className="button-wrapper">
             <DelayLink
              to="/page/about"
              className="button"
              delay={900}
              >
              more on us
            </DelayLink>

          </div>
        </div>
        <div className="projects">
          <ProjectItems />
        </div>
      </div>
    );
  }
}

export default withApollo(Home);
