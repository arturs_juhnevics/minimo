import React from 'react';
import DelayLink from './elements/DelayLink';
import { connect } from 'react-redux';
import { pageLoading } from '../actions/pageActions';

class NotFoundPage extends React.Component{
    componentDidMount(){
        console.log("asd");
        this.props.pageLoading(false); 
    }
    render(){
        return <div className="notfoundpage">
            <p className="heading"><span className="oops">OOPS!</span></p>
            <p className="text"> We did not find this page</p>
            <p>
                <DelayLink
                    to={"/"}
                    className="button"
                    delay={900}
                    >
                    Go to home
                </DelayLink>
            </p>
          </div>;
    }
}

const mapStateToProps = state => ({
    page: state.page
})
  
export default connect(mapStateToProps, { pageLoading })(NotFoundPage);
  