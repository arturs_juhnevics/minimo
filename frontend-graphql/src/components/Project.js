import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Button from './elements/Button';
import Masonry from 'react-masonry-css'
import { connect } from 'react-redux';
import { pageLoading } from '../actions/pageActions';

const PROJECT_SINGLE_QUERY = gql`
  query GetProjects($filter: String!){
      projectBy(uri: $filter){
      	title
      	projectInfo {
	      projectDescription
	      workDone {
	        done
	      }
	      url
	      gallery{
          id
	      	sourceUrl
	      }
	        
	    }
      }
  }
`;

function WorkDone(props){
	return <span>{props.item.done}</span>;
}

function Gallery(props){
  return <div className="gallery__image"><img alt={props.item.title} src={props.item.sourceUrl} /></div>;
}


class Project extends Component {
  state = {
    project: [],
    projectDescription: "",
    workDone: [],
    url: "",
    gallery: [],
  };


	componentDidMount() {
    this.executeProjectSingleQuery();
  }

  	executeProjectSingleQuery = async () => {
  		const { match, client } = this.props;
  		const filter = match.params.slug;
  		const result = await client.query({
	      query: PROJECT_SINGLE_QUERY,
	      variables: { filter },
	    });
	    const project = result.data.projectBy;
      const projectDescription = project.projectInfo.projectDescription;
      const workDone = project.projectInfo.workDone;
      const url = project.projectInfo.url;
      const gallery = project.projectInfo.gallery;
	    this.setState({ project, projectDescription, workDone, url, gallery });
      this.props.pageLoading(false); 

    };
    
    
  	render(){

      const breakpointColumnsObj = {
        default: 2,
        1100: 2,
        700: 1,
        500: 1
      };

  		const { project, projectDescription, workDone, url, gallery } = this.state;
  		return(
  			<div className="single-project">
          <div className="single-project__heading">
            <h1>{project.title}</h1> 
            <Button target="_blank" href={url} name="Launch website" />;
          </div>
  				
          <div className="work-done">
            {workDone.map(done => (
                <WorkDone key={done.done} item={done} />
            ))}
          </div>

          <p className="description" >{projectDescription}</p>

  				<Masonry 
            breakpointCols={breakpointColumnsObj}
            className="gallery"
            columnClassName="gallery-column">
            {gallery.map(item => (
              <Gallery key={item.id} item={item} />
            ))}
          </Masonry>

  			</div>
  		)
  	}
}

const mapStateToProps = state => ({
  page: state.page
})

export default withApollo( connect(mapStateToProps, { pageLoading })(Project));