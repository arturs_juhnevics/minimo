import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { withRouter } from 'react-router'
import Header from './Header';
import Home from './Home';
import Page from './Page';
import Project from './Project';
import Work from './Work';
import Footer from "./Footer"
import NotFoundPage from "./NotFoundPage"
import store from '../store';
import { connect } from 'react-redux';
import { pageLoading } from '../actions/pageActions';

class App extends Component {
  state = {
    loading: false,
    previousLocation: this.props.location.pathname
  }
  
  componentDidMount(){
    const storeState = store.getState();
    const { loading } = this.state;

    if (storeState.page.loading === false && loading === false) {

      setTimeout(() => {
            this.setState({
            loading: true,
          })
        }, 900);
    }

    this.props.history.listen((location, action) => {

      this.setState({
        loading: false
      })
      window.scrollTo(0, 0);  

    });
  }

  render(){
    const storeState = store.getState();
    return (
      

      <div className="center">
        <Header loading={storeState.page.loading} navState={storeState.page.navClosed}/>
        <div className="container main-content">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/page/work" component={Work}  />
            <Route path="/page/:slug" component={Page}  />
            <Redirect from='/page/Home' to='/' />
            <Route exact path="/project/:slug" component={Project} />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
        <Footer />       
      </div>
    )

  }
}

const mapStateToProps = state => ({
  page: state.page
})

export default withRouter( connect(mapStateToProps, { Header, pageLoading })(App))
