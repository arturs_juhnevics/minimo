import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';

import DelayLink from './elements/DelayLink';

import { pageLoading } from '../actions/pageActions';
import { connect } from 'react-redux';

const PROJECT_QUERY = gql`
  query {projects(where:{orderby:{field: MENU_ORDER, order: ASC}}){
      edges{
        node {
          title
          link
          slug
          featuredImage {
 
            mediaDetails {
  
              sizes {
                file
                height
                mimeType
                name
                sourceUrl
                width
              }
            }
          }
        }
      }
    }
  }
`;

class ProjectItem extends Component {

	state = {
	    projects: [],
	 };

	componentDidMount() {
    	this.executeProjectQuery();
  	}

  	executeProjectQuery = async () => {
  		const { client } = this.props;
  		const result = await client.query({
	      query: PROJECT_QUERY,
	    });
	    const projects = result.data.projects.edges;
	    this.setState({ projects });
      this.props.pageLoading(false); 
	    
  	};

  	render(){
  		const { projects } = this.state;
  		return(
  			<div className="projects__list">
  				{projects.map(project => (
  					<DelayLink className="link" key={project.node.slug} to={'/project/' + project.node.slug} delay={900}>
	  					<div className="projects__item">
	  						<h2>{project.node.title}</h2>
	  						<img alt={project.node.title} src={project.node.featuredImage.mediaDetails.sizes['3'].sourceUrl} />
	  					</div>
  					</DelayLink>
  				))}
  			</div>
  		)
  	}
}

const mapStateToProps = state => ({
  page: state.page
})

export default withApollo( connect(mapStateToProps, { pageLoading })(ProjectItem));