import React from 'react';
import { ReactComponent as Logo } from '../static/images/postlight-logo.svg';

const Footer = () => (
	<div className="footer container">
		<p>For more information and prices write us at <a className="email" href="mailto:arturs@minimo.lv">arturs@minimo.lv</a></p>
		<p className="copyright">© Minimo Digital Agency 2020</p>
	</div>
);

export default Footer;
