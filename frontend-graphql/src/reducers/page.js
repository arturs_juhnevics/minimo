import { PAGE_LOADING } from '../actions/types';
import { NAVIGATION } from '../actions/types';

const initialState = {
	loading: true,
	navClosed: true,
}

export default function(state = initialState, action) {
	switch (action.type) {
		case PAGE_LOADING:
			return {
				loading: action.data
			}
		case NAVIGATION:
			return {
				navClosed: action.data
			}
		default:
			return state;

	}

}