import { PAGE_LOADING } from './types';


export const pageLoading = postData => dispatch => {
  const action = {
    type: PAGE_LOADING,
    data: postData
  }
  dispatch(action)
}