import { NAVIGATION } from './types';


export const navState = postData => dispatch => {
  const action = {
    type: NAVIGATION,
    data: postData
  }
  dispatch(action)
}