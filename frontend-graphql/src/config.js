// graphql api url localhost
let url = 'http://minimo.localtest.me/graphql';

// If we're running on live server.
if(window.location.hostname != 'localhost'){
 url = "http://admin.minimo.lv/graphql";
}
// If we're running on Docker, use the WordPress container hostname instead of localhost.
if (process.env.HOME === '/home/node') {
  url = 'http://minimo.localtest.me/graphql';
}

const Config = {
  gqlUrl: url,
};

export default Config;
