gulp = require 'gulp'
uglify = require 'gulp-uglify'
minifyCss = require 'gulp-clean-css'
stripCssComments = require 'gulp-strip-css-comments'
sass = require 'gulp-sass'
sourcemaps = require 'gulp-sourcemaps'
autoprefixer = require 'gulp-autoprefixer'

AUTOPREFIXER_BROWSERS = [
  'ie >= 9'
  'ie_mob >= 10'
  'ff >= 3.5'
  'chrome >= 4.0'
  'safari >= 3.1'
  'opera >= 10.5'
  'ios >= 7'
  'android >= 4.4'
  'bb >= 10'
]

swallowError = (error) ->
  console.log error.toString()
  @emit 'end'
  return



paths =
  build:
    css: '../src/styles'
  watch:
    sass:[ 
      '../src/sass/*.scss'
      '../src/sass/**/*.scss'
    ]
  source:
    sass: [
      '../src/sass/main.scss'
    ]


gulp.task 'sass', ->
  gulp.src paths.source.sass
    .pipe sourcemaps.init({loadMaps:true})
    .pipe sass()
      .on('error', swallowError)
    .pipe stripCssComments()
    .pipe autoprefixer({browsers: AUTOPREFIXER_BROWSERS})
    .pipe minifyCss()
      .on('error', swallowError)
    .pipe sourcemaps.write('/')
    .pipe gulp.dest paths.build.css


gulp.task 'watch', ->

  gulp.watch paths.watch.sass, ['sass']
